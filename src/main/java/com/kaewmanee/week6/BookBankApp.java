package com.kaewmanee.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank kaewmanee = new BookBank("Kaewmanee",100.0); //Default constructor
        kaewmanee.deposit(50);
        kaewmanee.print();

        BookBank pronchita = new BookBank("Pronchita",1000.0);
        pronchita.deposit(100000);
        pronchita.print();
        pronchita.withdraw(1000);
        pronchita.print();

        BookBank kitiyapron = new BookBank("kitiyapron",10.0);
        kitiyapron.print();

    }
    
}
