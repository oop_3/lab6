package com.kaewmanee.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot judy = new Robot ("Judy",'J',1,0);
        Robot petter = new Robot("Petter",'P',10,10);
        judy.print();
        judy.right();
        judy.print();
        judy.up();
        judy.print();
        judy.right();
        judy.print();
        judy.down();
        judy.print();
        judy.down();
        judy.print();
        petter.print();

        for (int y = Robot.Y_MIN;y<=Robot.Y_MAX;y++){
            for (int x = Robot.Y_MIN;x<=Robot.Y_MAX;x++){
                if (judy.getX() == x && judy.getY() == y){
                    System.out.print(judy.getSymbol());
                }
                else if (petter.getX() == x && petter.getY() == y){
                    System.out.print(petter.getSymbol());
                    
                }
                else{
                    System.out.print("-");
                }
            }
            System.out.println();
        }

    }
    
}
