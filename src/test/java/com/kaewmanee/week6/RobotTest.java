package com.kaewmanee.week6;

import java.beans.Transient;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());

    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());

    }
    @Test
    public void shouldRobotUpSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(2);
        assertEquals(true, result);
        assertEquals(9, robot.getY());

    }

    @Test
    public void shouldRobotUpSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());

    }

    @Test
    public void shouldRobotUpFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(false,result);
        assertEquals(0, robot.getY());

    }


    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN); // (10,0)
        boolean result = robot.up();// (10,-1)
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());

    }

    @Test
    public void shouldRobotUpBeforeMinSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN + 1);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(Robot.Y_MIN, robot.getY());

    }

    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());

    }

    @Test
    public void shouldRobotDownSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(2);
        assertEquals(true, result);
        assertEquals(13, robot.getY());

    }

    @Test
    public void shouldRobotDownSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(16, robot.getY());

    }

    @Test
    public void shouldRobotDownFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 19);
        boolean result = robot.down(8);
        assertEquals(false,result);
        assertEquals(Robot.Y_MAX, robot.getY());

    }

    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());

    }

    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX - 1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());

    }

    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left();
        assertTrue(result);
        assertEquals(9, robot.getX());

    }


    @Test
    public void shouldRobotLeftSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(2);
        assertEquals(true, result);
        assertEquals(8, robot.getX());

    }


    @Test
    public void shouldRobotLeftSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());

    }

    @Test
    public void shouldRobotLeftFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(11);
        assertEquals(false,result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotLeftFailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 11);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());

    }

    @Test
    public void shouldRobotLeftBeforeMinSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN + 1, 11);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }



    @Test
    public void shouldRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX());
    }

    
    @Test
    public void shouldRobotRightSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(2);
        assertEquals(true, result);
        assertEquals(12, robot.getX());

    }


    @Test
    public void shouldRobotRightSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getX());

    }

    
    @Test
    public void shouldRobotRightFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(11);
        assertEquals(false,result);
        assertEquals(Robot.X_MAX, robot.getX());
    }


    @Test
    public void shouldRobotRightAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 10);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());

    }

    @Test
    public void shouldRobotLeftBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX - 1, 11);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }
}
